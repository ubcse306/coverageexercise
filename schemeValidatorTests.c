#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "CUnit.h"
//#include "Automated.h"
#include "Basic.h"
#include "schemeValidator.h"

void test01(void) {
  int expected = -1;
  int actual = schemeValidator("4");
  CU_ASSERT_EQUAL(expected,actual);
}

void test02(void) {
  int expected = 0;
  int actual = schemeValidator("http");
  CU_ASSERT_EQUAL(expected,actual);
}

void test03(void) {
  int expected = 1;
  int actual = schemeValidator("HTTP");
  CU_ASSERT_EQUAL(expected,actual);
}

void alphonceTest04(void) {
  int expected = 0;
  int actual = schemeValidator("ftp");
  CU_ASSERT_EQUAL(expected,actual);
}

void alphonceTest05(void) {
  _Bool expected = true;
  _Bool actual = isAlpha('t');
  CU_ASSERT_EQUAL(expected,actual);
}

int main() {
  CU_pSuite Suite = NULL;
  if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }
  Suite = CU_add_suite("Suite_of_tests_with_valid_inputs", NULL, NULL);
  if (NULL == Suite) {
    CU_cleanup_registry();
    return CU_get_error();
  }
  if ( (    NULL == CU_add_test(Suite, "message", test01) 
	 || NULL == CU_add_test(Suite, "message", test02) 
	 || NULL == CU_add_test(Suite, "message", test03) 
	) ) {
    CU_cleanup_registry();
    return CU_get_error();
  }
  //  CU_set_output_filename("test");
  //  CU_automated_run_tests();
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
